package me.perryplaysmc.plugin.commands.server_admin;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.CommandSource;
import org.bukkit.Bukkit;

import java.text.DecimalFormat;

@SuppressWarnings("all")
public class CommandReload extends Command {
    public CommandReload() {
        super("reload", "rl", "pluginrl", "pluginreload", "plrl");
        setDescription("Reloads plugins and configurations");
    }
    
    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        hasPermission("reload");
        long reloadStart = System.currentTimeMillis();
        CoreAPI.broadcastPerm("reload.see", "[p]Reload in progress");
        Bukkit.reload();
        CoreAPI.broadcastPerm("reload.see", "[p]Reload complete took[pc] " +
                                       getFormatedTimeMillis(System.currentTimeMillis() - reloadStart, false, true));
    }
    
    
    String getFormatedTimeMillis(long millis, boolean concat, boolean showDHMS) {
        long seconds = millis / 1000L;
        long minutes = seconds / 60L;
        long hours = minutes / 60L;
        long days = hours / 24L;
        long weeks = days / 7L;
        long months = days / 30L;
        long years = months / 12L;
        
        millis -= seconds * 1000L;
        seconds -= minutes * 60L;
        minutes -= hours * 60L;
        hours -= days * 24L;
        days -= weeks * 7L;
        days -= months * 30L;
        
        String min = minutes > 1L ? " Minutes" : " Minute";
        String se = seconds > 1L ? " Seconds" : " Second";
        String hr = hours > 1L ? " Hours" : " Hour";
        String day = days > 1L ? " Days" : " Day";
        
        String secondsStr = seconds + "";
        
        long m = minutes;long h = hours;long d = days;
        
        StringBuilder sb = new StringBuilder();
        if (years > 0) {
            sb.append(years + (showDHMS ? (years > 1 ? " Years" : " Year") : ""));
            if ((months > 0) || (weeks > 0) || (days > 0) || (hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (months > 0) {
            sb.append(months + (showDHMS ? (months > 1 ? " Months" : " Month") : ""));
            if ((weeks > 0) || (days > 0) || (hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (weeks > 0) {
            sb.append(weeks + (showDHMS ? (weeks > 1 ? " Weeks" : " Week") : ""));
            if ((days > 0) || (hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (days > 0) {
            sb.append(days + (showDHMS ? (days > 1 ? " Days" : " Day") : ""));
            if ((hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (hours > 0) {
            sb.append(hours + (showDHMS ? (hours > 1 ? " Hours" : " Hour") : ""));
            if ((minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (minutes > 0) {
            sb.append(minutes + (showDHMS ? (minutes > 1 ? " Minutes" : " Minute") : ""));
            if (seconds > 0) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (seconds > 0) {
            sb.append(secondsStr);
            if (millis > 0) {
                sb.append(".");
            }else {
                sb.append((showDHMS ? (seconds > 1 ? " Seconds" : " Second") : ""));
            }
        }
        if (millis > 0) {
            DecimalFormat f = new DecimalFormat("##");
            if (seconds <= 0L) {
                sb.append("0.");
            }
            sb.append(f.format(millis) + (showDHMS ? " Seconds" : ""));
        }
        
        return sb.toString();
    }
    
}
