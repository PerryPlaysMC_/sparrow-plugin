package me.perryplaysmc.plugin.commands.util.chat;

import com.google.common.collect.Lists;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.ListsHelper;

import java.util.HashMap;
import java.util.List;

@SuppressWarnings("all")
public class CommandMessage extends Command {
    
    private HashMap<String, String> replies;
    
    public CommandMessage() {
        super("message", "msg", "tell", "r", "reply"
                , "mmessage", "mmsg", "mtell", "mr", "mreply", "w", "whisper"
                , "minecraft:message", "minecraft:msg", "minecraft:tell", "minecraft:r", "minecraft:reply", "minecraft:w", "minecraft:whisper");
        replies = new HashMap<>();
        setDescription("Message someone!");
    }
    
    @Override
    public void run(CommandSource s, CommandLabel cl, Argument[] args) throws CommandException {
        if(cl.equalsIgnoreCase("r", "reply", "mr", "mtell", "mreply", "minecraft:r", "minecraft:reply")) {
            hasPermission("reply");
            if(args.length > 0) {
                CommandSource r = s.getReplier();
                checkUser(r);
                String msg = createMessage(0);
                String x = CoreAPI.format("Commands.Message.Sender.Message", msg, r.getName(), s.getName());;
                Message message = new Message(x.contains(" "+msg) ? x.split(" "+msg)[0] : x.split(msg)[0]);
                if(CoreAPI.getMessages().isSet("Commands.Message.Sender.hover"))
                    message.tooltip(CoreAPI.formatArray("Commands.Message.Sender.hover", msg, r.getName(), s.getName()));
                if(CoreAPI.getMessages().isSet("Commands.Message.Sender.insert"))
                    message.suggest(ColorUtil.removeColor(CoreAPI.format("Commands.Message.Sender.insert", msg, r.getName(), s.getName())));
                message.then(x.contains(" " + msg) ? " " : "");
                message.then(true, msg);
                message.send(s);
                
                String rsp = CoreAPI.format("Commands.Message.Target.Message", msg, s.getName(), r.getName());
                Message respond = new Message(rsp.contains(" " + msg) ? rsp.split(" " + msg)[0] : rsp.split(msg)[0]);
                if(CoreAPI.getMessages().isSet("Commands.Message.Target.hover"))
                    respond.tooltip(CoreAPI.formatArray("Commands.Message.Target.hover", msg, s.getName(), r.getName()));
                if(CoreAPI.getMessages().isSet("Commands.Message.Sender.insert"))
                    respond.suggest(ColorUtil.removeColor(CoreAPI.format("Commands.Message.Target.insert", msg, s.getName(), r.getName())));
                respond.then(rsp.contains(" " + msg) ? " " : "");
                respond.then(true, msg);
                respond.send(r);
                return;
            }else {
                argsLengthError(false);
            }
            return;
        }
        if(args.length > 1) {
            hasPermission("message");
            CommandSource r = getSource(args[0]);
            
            
            String msg = createMessage(1);
            String x = CoreAPI.format("Commands.Message.Sender.Message", msg, r.getName(), s.getName());;
            Message message = new Message(x.contains(" "+msg) ? x.split(" "+msg)[0] : x.split(msg)[0]);
            if(CoreAPI.getMessages().isSet("Commands.Message.Sender.hover"))
                message.tooltip(CoreAPI.formatArray("Commands.Message.Sender.hover", msg, r.getName(), s.getName()));
            if(CoreAPI.getMessages().isSet("Commands.Message.Sender.insert"))
                message.suggest(ColorUtil.removeColor(CoreAPI.format("Commands.Message.Sender.insert", msg, r.getName(), s.getName())));
            message.then(x.contains(" " + msg) ? " " : "");
            message.then(true, msg);
            message.send(s);
            
            String rsp = CoreAPI.format("Commands.Message.Target.Message", msg, r.getName(), s.getName());
            Message respond = new Message(rsp.contains(" " + msg) ? rsp.split(" " + msg)[0] : rsp.split(msg)[0]);
            if(CoreAPI.getMessages().isSet("Commands.Message.Target.hover"))
                respond.tooltip(CoreAPI.formatArray("Commands.Message.Target.hover", msg, r.getName(), s.getName()));
            if(CoreAPI.getMessages().isSet("Commands.Message.Target.insert"))
                respond.suggest(ColorUtil.removeColor(CoreAPI.format("Commands.Message.Target.insert", msg, r.getName(), s.getName())));
            respond.then(rsp.contains(" " + msg) ? " " : "");
            respond.then(true, msg);
            respond.send(r);
            s.setReplier(r);
            r.setReplier(s);
        }else {
            argsLengthError(true);
        }
    }
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel cl, Argument[] args) {
        List<String> f = Lists.newArrayList();
        f.addAll(loadTab(1, ListsHelper.createList(CoreAPI.getUserNames()).add("Console")));
        return f;
    }
}
