package me.perryplaysmc.plugin.commands.util.world;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/10/19-2023
 * Package: me.perryplaysmc.plugin.commands.util.world
 * Class: CommandSetSpawn
 * <p>
 * Path: me.perryplaysmc.plugin.commands.util.world.CommandSetSpawn
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandSetSpawn extends Command {
    
    public CommandSetSpawn() {
        super("setspawn");
        setDescription("Sets the world spawn");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        hasPermission("setspawn");
        mustBe(true);
        if(args.length>0) {
            argsLengthError(true);
            return;
        }
        User u = s.getUser();
        CoreAPI.getLocations().set("Spawns." + u.getWorld().getName(), u.getLocation());
        sendMessage("Spawn.set", u.getWorld().getName());
    }
    
}
