package me.perryplaysmc.plugin.commands.util.world;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import org.bukkit.block.Block;

import java.util.ArrayList;

public class CommandUpdateBlocks extends Command {
    public CommandUpdateBlocks() {
        super("updateblocks", "ubs");
    }

    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        if(args.length == 1) {
            int amt = integer(args[0]);
            for(Block b : CoreAPI.getBlockHelper().getConnectedBlocks(amt, s.getUser().getLocation().getBlock(), new ArrayList<>())) {
                b.getState().update(true, true);
            }
        }
    }
}
