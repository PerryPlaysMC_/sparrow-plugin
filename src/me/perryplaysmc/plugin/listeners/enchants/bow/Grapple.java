package me.perryplaysmc.plugin.listeners.enchants.bow;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.enchantment.EnchantHandler;
import me.perryplaysmc.utils.inventory.ingorish.ItemEnchant;
import me.perryplaysmc.user.User;
import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
public class Grapple implements Listener {


    List<Player> noFall = new ArrayList<>();

    private boolean isFinite(double d) {
        return Math.abs(d) <= 1.7976931348623157E308D;
    }

    @EventHandler
    void onProjectileHit(ProjectileHitEvent e) {
        if(e.getEntity().getShooter() instanceof Player) {
            if(e.getEntity() instanceof Arrow) {
                if(e.getEntity().hasMetadata("Grapple")) {
                    Location t = e.getHitEntity() != null ? e.getHitEntity().getLocation() : e.getHitBlock() != null ? e.getHitBlock().getLocation() : e.getEntity().getLocation();
                    Player p = (Player) e.getEntity().getShooter();
                    User u = CoreAPI.getUser(p.getUniqueId());
                    u.getAntiCheatDetector().setIgnoreSpeed(true);
                    if(t.getY() > p.getLocation().getY())
                        p.teleport(p.getLocation().clone().add(0,0.5,0));
                    Vector v = getVectorForPoints(p.getLocation(), t);
                    if(!isFinite(v.getX()) || !isFinite(v.getY()) || !isFinite(v.getZ())) return;
                    p.setVelocity(v);
                    if(!noFall.contains(p)) noFall.add(p);
                }
            }
        }
    }

    @EventHandler
    void onDamage(EntityDamageEvent e) {
        if(!(e.getEntity() instanceof Player)) return;
        if(e.getCause() == EntityDamageEvent.DamageCause.FALL) {
            if(noFall.contains((Player)e.getEntity())) {
                noFall.remove((Player)e.getEntity());
                User u = CoreAPI.getUser(e.getEntity().getUniqueId());
                u.getAntiCheatDetector().setIgnoreSpeed(false);
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    void onProjectileLaunch(ProjectileLaunchEvent e) {
        if(e.getEntity().getShooter() instanceof Player) {
            if(e.getEntity() instanceof Arrow) {
                User u = CoreAPI.getUser(((Player) e.getEntity().getShooter()).getPlayer());
                ItemEnchant i = new ItemEnchant(u.getItemInHand());
                if(EnchantHandler.getEnchantment("Grapple") != null)
                    if(i.hasEnchant(EnchantHandler.getEnchantment("Grapple"))) {
                        FixedMetadataValue v = new FixedMetadataValue(Core.getAPI(), true);
                        e.getEntity().setMetadata("Grapple", v);
                    }
            }
        }
    }


    private Vector getVectorForPoints(Location l1, Location l2) {
        double g = -0.08;
        double d = l2.distance(l1);
        double t = d;
        double vX = (1.0+0.07*t) * (l2.getX() - l1.getX())/t;
        double vY = (1.0+0.03*t) * (l2.getY() - l1.getY())/t - 0.5*g*t;
        double vZ = (1.0+0.07*t) * (l2.getZ() - l1.getZ())/t;
        return new Vector(vX, vY, vZ);
    }

}
